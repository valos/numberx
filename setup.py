# -*- coding: utf-8 -*-

# NumberX -- A mathematical puzzle game
#
# Copyright (C) 2009 Valéry Febvre <vfebvre@easter-eggs.com>
# http://code.google.com/p/numberx/
#
# This file is part of NumberX.
#
# NumberX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# NumberX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from distutils.core import setup

def main():
    setup(name         = 'numberx',
          version      = '1.0.1',
          description  = 'A mathematical puzzle game',
          author       = 'Valery Febvre',
          author_email = 'vfebvre@easter-eggs.com',
          url          = 'http://code.google.com/p/numberx/',
          classifiers  = [
            'Development Status :: 2 - Beta',
            'Environment :: X11 Applications',
            'Intended Audience :: End Users/Phone UI',
            'License :: GNU General Public License (GPL)',
            'Operating System :: POSIX',
            'Programming Language :: Python',
            'Topic :: Desktop Environment',
            ],
          packages     = ['numberx'],
          scripts      = ['numberx/numberx'],
          data_files   = [
            ('share/applications', ['data/numberx.desktop']),
            ('share/pixmaps', ['data/numberx.png']),
            ('share/numberx', ['README']),
            ],
          )

if __name__ == '__main__':
    main()
