     _   _                 _             __  __
    | \ | |_   _ _ __ ___ | |__   ___ _ _\ \/ /
    |  \| | | | | '_ ` _ \| '_ \ / _ \ '__\  /
    | |\  | |_| | | | | | | |_) |  __/ |  /  \
    |_| \_|\__,_|_| |_| |_|_.__/ \___|_| /_/\_\


Description
===========
NumberX is a mathematical puzzle game that will challenge your mental
math abilities!

Four random numbers, between 1 and 9, you are given and you must put
them all together using the basic arithmetic operators in such a way as
to arrive at a given target random number.

Every puzzle has a solution.
The difficulty of the puzzles varies, some are easy and others are much
more difficult!

https://gitlab.com/valos/numberx/

Licensed under the GNU General Public License v3.


Authors
=======
Valéry Febvre <vfebvre@easter-eggs.com>


Requirements
============
- python-elementary
- python-math


Notes
=====
- NumberX is tested on SHR unstable only.
  Normally, it should run on any system with a revision of
  python-elementary equal or greater to 40756.


Report Bugs
===========
If there is something wrong, please use to the Google code issues:
https://gitlab.com/valos/numberx/issues
Report a bug or look there for possible workarounds.


Version History
===============

2009-11-30 - Version 1.0.1
--------------------------
- Fixed callbacks with new python-elementary (revision >= 43900)

2009-11-04 - Version 1.0.0
--------------------------
- First release.
