DESCRIPTION = "A mathematical puzzle game"
HOMEPAGE = "http://code.google.com/p/numberx/"
LICENSE = "GPLv3"
AUTHOR = "Valery Febvre <vfebvre@easter-eggs.com>"
SECTION = "x11/applications"
PRIORITY = "optional"
DEPENDS = "python-native"

PV = "1.0.1+svnr${SRCPV}"

S = "${WORKDIR}/trunk"

PACKAGE_ARCH = "all"

SRC_URI = "svn://numberx.googlecode.com/svn;module=trunk;proto=http"

inherit distutils

FILES_${PN} += "${datadir}/numberx ${datadir}/applications/numberx.desktop ${datadir}/pixmaps"

RDEPENDS += "python-math python-elementary" 
